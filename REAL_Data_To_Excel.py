import openpyxl
import os


def clock_time_to_time(clock_time):
	### Converts clock time to pure seconds

	time_list = clock_time.split(':')
	for i in range(0, len(time_list)):
		time_list[i] = float(time_list[i]) #Converts the hrs, min, and sec parts of the clock from strings to floats
	time_list[0] = time_list[0] * 3600 #Convert hrs to sec
	time_list[1] = time_list[1] * 60 #Convert min to sec
	
	return sum(time_list) #Adds converted hrs, min, and sec to get total time


def move_data(ws, fp):
	### Moves all values from the .data file into the Excel worksheet

	ws['A1'] = "Frequency (kHz)"
	ws['B1'] = "Final Raw Voltage (V)"
	ws['C1'] = "Final Normalized Voltage (V)"
	ws['D1'] = "Clock Time"
	ws['E1'] = "Initial Raw Voltage (V)" 
	ws['F1'] = "Time Converted (sec)"
	ws["G1"] = "Time Difference (sec)"

	# Fills columns D, E, and F
	row_num = 2
	for line in fp:
		input_line = line.split(',')
		ws['D' + str(row_num)] = input_line[0] #Puts the clock time (first part of csv value) in column D

		time = clock_time_to_time(input_line[0])

		ws['F' + str(row_num)] = time #Puts seconds representation of clock time in column F
		ws['E' + str(row_num)] = float(input_line[1]) #Puts the raw voltage (second part of csv value) in column E
		row_num += 1
	
	#Fills in column G by taking time elapsed between each data point
	for x in range(3, row_num):
		ws['G' + str(x)] = ws['F' + str(x)].value - ws['F' + str(x - 1)].value
	
	# Fills column A by converting time to frequency
	total_time = ws['F' + str(row_num - 1)].value - ws['F2'].value + 0.05 #Finds the total time the well was sensed
	ws['A2'] = 100
	for x in range(3, row_num):	
		ws['A' + str(x)] = ws['A' + str(x-1)].value + ws['G' + str(x)].value/total_time * 9900


def find_new_voltage_cycle(voltage_column):
	### Finds the index of the point where the frequency shifts from 10 MHz to 100 kHz

	#Gets the avg of the last 10 voltage measurements
	rolling_avg_previous = 0
	for i in range(len(voltage_column) - 1, len(voltage_column) - 11, -1):
		rolling_avg_previous += voltage_column[i].value
	rolling_avg_previous /= 10
	
	#Goes through all voltage measurements in rvs - if one point is greater than the rolling avg of the 10 points behind it,
	#the avg of the next 10 points is compared that rolling avg. If it is at least 9% greater than that avg, that marks the 
	#location of voltage shift
	for i in range(len(voltage_column) - 11, 12, -1):
		rolling_avg_next = 0
		if voltage_column[i].value - voltage_column[i-1].value < -0.07 * rolling_avg_previous: #If point is greater than avg of 10 points behind it
			curr_forward = i - 1
			#Finds the avg of the next 10 points
			for j in range(10):
				rolling_avg_next += voltage_column[curr_forward].value
				curr_forward -= 1
			rolling_avg_next /= 10
			if rolling_avg_previous - rolling_avg_next < -0.07 * rolling_avg_previous:
				return i
		rolling_avg_previous -= voltage_column[i + 10].value / 10
		rolling_avg_previous += voltage_column[i].value / 10
	
	return 2 #if function can't find shift point, return the index of the first point


def shift_voltage(voltage_column, index, ws):
	### Shifts the raw voltages so that the 100 kHZ measurement is the first point in the column

	#Moves the raw voltage measurements from position of the shift to the end of column to column B
	j = 2
	for i in range(index, len(voltage_column)):
		ws['B' + str(j)] = voltage_column[i].value
		j += 1

	#Moves the rest of the raw voltage points to column B
	offset = j - 1
	for i in range(1, len(voltage_column) - offset + 1):
		ws['B' + str(j)] = voltage_column[i].value
		j+=1


def correct_raw_voltage(voltage_column, ws):
	### Shifts the raw voltage so that the raw voltage that corresponds to 100 kHZ is first in the column
	### We expect there to be a shift in every .data file, however if the program cannot detect a shift, it will keep the order the same

	new_cycle_index = find_new_voltage_cycle(voltage_column)
	shift_voltage(voltage_column, new_cycle_index, ws)


def remove_v_outliers(raw_voltage_column, modified_voltage_column):
	### Removes outliers by comparing them to a rolling avg of the previous 10 points
	
	#Finds the avg of the first 10 voltage values ASSUMING THEY HAVE NO OUTLIERS and copies them to a new column
	rolling_avg = 0
	for i in range(1, 11):
		rolling_avg += raw_voltage_column[i].value
		modified_voltage_column[i].value = raw_voltage_column[i].value
	rolling_avg /= 10

	#Checks if each voltage is within 8% of the avg of the 10 points behind it. If it is, copy to new column.
	#If not, copy the rolling avg in the new column instead
	for i in range(11, len(raw_voltage_column)):
		if raw_voltage_column[i].value > rolling_avg * 1.08 or raw_voltage_column[i].value < rolling_avg / 1.08:
			modified_voltage_column[i].value = rolling_avg
		else:
			modified_voltage_column[i].value = raw_voltage_column[i].value
		
		rolling_avg -= modified_voltage_column[i - 10].value / 10
		rolling_avg += modified_voltage_column[i].value / 10


def main():	
	dirpath = os.getcwd() # Experiment folders must be in same directory as this program.
	for experiment_name in os.listdir(dirpath):
		if os.path.isdir(experiment_name): 
			wb = openpyxl.Workbook() #Create a new workbork for every experiment folder
			for filename in os.listdir(experiment_name):
				ws = wb.create_sheet(filename) #Each .data file in each experiment folder gets its own sheet in the workbook
				fp = open(experiment_name + '/' + filename, 'r')
				move_data(ws, fp)
				correct_raw_voltage(ws['E'], ws)
				remove_v_outliers(ws['B'], ws['C'])
			wb.save(experiment_name + ".xlsx")
			print(experiment_name, "has been completed")

main()