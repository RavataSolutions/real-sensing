Name: REAL_Data_To_Excel.py
Version: 1.1


Description: 
REAL_Data_To_Excel.py expects to be in the same directory as the experiment folders. 
Experiment folders should contain all the .data files that were generated when sensing using the chip.
The program will create an Excel workbook for each experiment folder, and each .data file within the folder will receive its own sheet.
Each Excel workbook will be saved in the same directory as the program.
.data files should be in format clock_time,voltage
The program will move all csv values in a .data file to the sheet, then convert the clock_time to freq and
shift the voltage values so that the value that corresponds to 100kHz will be first in the column


System Requirements:
Python 3


Known Bugs:
If there is no shift or shift is undetected by find_new_voltage_cycle() function, program will crash. A necessary shift is expected in 
every .data file.


Change Log: